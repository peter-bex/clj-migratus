# clj-migratus
[![Clojars Project](https://img.shields.io/clojars/v/orangefoxcollective/clj-migratus.svg)](https://clojars.org/orangefoxcollective/clj-migratus)

[Migratus](https://github.com/yogthos/migratus) runner for [native Clojure projects](https://clojure.org/reference/deps_and_cli).

## Usage

Add the following to your `deps.edn`:

	:aliases
	{:migratus {:extra-deps {orangefoxcollective/clj-migratus {:mvn/version "0.1.0"}}             
	            :main-opts ["-m" "clj-migratus.core"]}}


Create a [Migratus configuration](https://github.com/yogthos/migratus#configuration) file `migratus.edn`:

    {:store :database
     :migration-dir "migrations"
     :db {:classname "com.mysql.jdbc.Driver"
	      :subprotocol "mysql"
          :subname "//localhost/migratus"
          :user "root"
          :password ""}}

You can also create a configuration file called `migratus.clj` if you
want to make use of the recommended way of reading configuration from
the environment instead of hardcoding secrets in checked-in files.

Then run `clj-migratus` via the command line. For example:

    $ clj -Amigratus init

	$ clj -Amigratus migrate

	$ clj -Amigratus create create-user-table

See [Migratus Usage](https://github.com/yogthos/migratus#usage) for documentation on each command.

## License

Copyright © 2020 benjamin cassidy

Licensed under the Apache License, Version 2.0.
